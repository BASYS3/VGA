#Supress compiler warning about these settings by setting them
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]

#Clock
set_property PACKAGE_PIN W5 [get_ports clock]
set_property IOSTANDARD LVCMOS33 [get_ports clock]
    create_clock -add -name sys_clk_pin -period 10.00 -waveform {0 5} [get_ports clock]
	
#color select switches
set_property IOSTANDARD LVCMOS33 [get_ports colorSwitches[*]]
set_property PACKAGE_PIN V17 [get_ports colorSwitches[0]]
set_property PACKAGE_PIN V16 [get_ports colorSwitches[1]]
set_property PACKAGE_PIN W16 [get_ports colorSwitches[2]]

    
#red channel
set_property IOSTANDARD LVCMOS33 [get_ports red[*]]
set_property PACKAGE_PIN G19 [get_ports red[0]]
set_property PACKAGE_PIN H19 [get_ports red[1]]
set_property PACKAGE_PIN J19 [get_ports red[2]]
set_property PACKAGE_PIN N19 [get_ports red[3]]

#green channel
set_property IOSTANDARD LVCMOS33 [get_ports grn[*]]
set_property PACKAGE_PIN J17 [get_ports grn[0]]
set_property PACKAGE_PIN H17 [get_ports grn[1]]
set_property PACKAGE_PIN G17 [get_ports grn[2]]
set_property PACKAGE_PIN D17 [get_ports grn[3]]

#blue channel
set_property IOSTANDARD LVCMOS33 [get_ports blu[*]]
set_property PACKAGE_PIN N18 [get_ports blu[0]]
set_property PACKAGE_PIN L18 [get_ports blu[1]]
set_property PACKAGE_PIN K18 [get_ports blu[2]]
set_property PACKAGE_PIN J18 [get_ports blu[3]]

#Horizontal and Vertical sync lines
set_property IOSTANDARD LVCMOS33 [get_ports hs]
set_property IOSTANDARD LVCMOS33 [get_ports vs]
set_property PACKAGE_PIN P19 [get_ports hs]
set_property PACKAGE_PIN R19 [get_ports vs]

set_property IOSTANDARD LVCMOS33 [get_ports hs_led]
set_property IOSTANDARD LVCMOS33 [get_ports vs_led]
set_property PACKAGE_PIN U16 [get_ports hs_led]
set_property PACKAGE_PIN E19 [get_ports vs_led]