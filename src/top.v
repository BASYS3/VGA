`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06.12.2017 15:46:44
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


//top level module, complete program overview
module top(
    input clock,
	input[2:0] colorSwitches,
    output [3:0] red,
    output [3:0] grn,
    output [3:0] blu,
    output vs,
    output vs_led,
    output hs,
    output hs_led
    );
	
	reg[1:0] clk_div = 0;
	
	//parameter definitions for constants, taken from http://tinyvga.com/vga-timing/640x480@60Hz
	parameter DISP_WIDTH = 640;
	parameter DISP_HEIGHT = 480;
	parameter ROW_FP = 656;
	parameter ROW_SYNC = 752;
	parameter ROW_BP = 800;
	parameter COL_FP = 490;
	parameter COL_SYNC = 492;
	parameter COL_BP = 525;
	
	//parameters for pattern display
	parameter ROW_DIV_MAX = 9;
	parameter COL_DIV_MAX = 7;
	parameter BLU_DIVH_MAX = 160;
	parameter BLU_DIVV_MAX = 120;
	
    
    reg [3:0] blu_reg = 4'hF;
    reg [3:0] red_reg = 4'h0;
    reg [3:0] grn_reg = 4'h0;
    reg vs_reg = 1;
    reg hs_reg = 1;
    
    //Clock is 100MHz, try it.
    
    //need video memory
    //for now, try a blue screen
    assign blu = blu_reg;
    assign red = red_reg;
    assign grn = grn_reg;
    assign vs = vs_reg;
    assign vs_led = vs_reg;
    assign hs_led = hs_reg;
    assign hs = hs_reg;
    
    //for now, just try display a blue screen
    
    //row counter, overflow at DISP_WIDTH and fire HS, and increment colCounter
    reg[11:0] rowCounter = 0;
    //column counnter, overflow at DISP_HEIGHT and fire VS
    reg[11:0] colCounter = 0;
    
    reg[7:0] rowDivisor = 0;
	reg[7:0] colDivisor = 0;
	reg[3:0] redCounter = 0;
    
    reg[10:0] bluDivisorh = 0;
    reg[10:0] bluDivisorv = 0;
    reg[1:0] bluCounterv = 0;
    reg[1:0] bluCounterh = 0;
	
	
	//need to divide clock by 4 to get the 25 MHZ clock.
	always @(posedge clock) begin
		clk_div <= clk_div +1;
	end
    
    //display loop
    always @(negedge clk_div[1]) begin
    	rowCounter <= rowCounter +1 ;

		
    	//row counter logic
    	//visible for 1280 pixels (insert logic here)
    	if(rowCounter < DISP_WIDTH && colCounter < DISP_HEIGHT)begin    
    		//logic to produce green bars
			rowDivisor <= rowDivisor +1;
			bluDivisorh <= bluDivisorh +1;
			
			if(bluDivisorh == BLU_DIVH_MAX) begin
				bluDivisorh <= 0;
				bluCounterh <= bluCounterh +1;
			end
			
			if(bluDivisorv == BLU_DIVV_MAX) begin
				bluDivisorv <= 0;
				bluCounterv <= bluCounterv +1;
				redCounter <= 0;
				colDivisor <=0;
			end
			else if(colDivisor == COL_DIV_MAX) begin
				if(redCounter == 15)
					redCounter <= redCounter;
				else
					redCounter <= redCounter +1;
					
				colDivisor <= 0;
			end
			
			if(rowDivisor == ROW_DIV_MAX) begin
				rowDivisor <= 0;
				if(colorSwitches[0])
					grn_reg <= grn_reg +1;
				else
					grn_reg <= 4'h0;
			end
			
						
			
			//logic to produce red bars
			if(colorSwitches[1])
				red_reg <= redCounter;
			else
				red_reg <= 4'h0;
			
			if(colorSwitches[2])
				blu_reg <= {bluCounterv[1:0], bluCounterh[1:0]};
			else
				blu_reg <= 4'h0;
    	end
    	else begin
    		grn_reg <= 4'h0;
    		blu_reg <= 4'h0;
    		red_reg <= 4'h0;
    	end
    	
    	//front porch BEFORE sync
    	//do nothing for 48 pixels
    	
    	//sync signal, low for 112 pixels
    	if(rowCounter == ROW_FP)
    		hs_reg <= 0;
    	//release sync after the 112 pixels
    	if(rowCounter == ROW_SYNC)
    		hs_reg <= 1;
    		
    	//Back porch AFTER sync, 248 pixels
    	//do nothing for 248 pixels
    	
    	//at 1688 pixels, reset logic
    	if(rowCounter == ROW_BP) begin
    		rowCounter <= 0;
    		rowDivisor <= 0;
    		bluCounterh <=0;
    		bluDivisorh <=0;
    		colCounter <= colCounter +1;
    		bluDivisorv <= bluDivisorv +1;
			colDivisor <= colDivisor +1;
    	end
    	
    	//col counter logic
    	//visible for 1024 columns (insert Logic Here)
    	
    	//front porch BEFORE sync, 1 line
    	//do nothing for one line
    	
    	//vertical sync, low for 3 lines
    	if(colCounter == COL_FP)
    		vs_reg <= 0;
    		
    	if(colCounter == COL_SYNC)
    		vs_reg <= 1;
    		
    	//back porch AFTER sync, 38 lines
    	//do nothing for 38 lines
    	
    	//end of frame, reset col counter, go to top
    	if(colCounter == COL_BP) begin
    		colCounter <= 0;
    		bluDivisorv <= 0;
    		bluCounterv <= 0;
			redCounter <= 0;
			colDivisor <=0;
    	end
    		
	end
	
endmodule
